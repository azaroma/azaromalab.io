import React from "react"
import { Link } from "gatsby"
import "./nav.css"

const sections = [`azaroma`, `about`, `blog`]

const BrokenText = ({ word }) => (
  <>
    {word.split(``).map((char, i) => (
      <span key={`word-${char}-${i}`} className={`char-${i}`}>
        {char}
      </span>
    ))}
  </>
)

const NavLink = ({ className, title }) => (
  <li className="nav-link-container">
    <Link className={`nav-link ${className}`} to="/">
      <BrokenText word={title} />
    </Link>
  </li>
)

const Nav = () => {
  const first = sections.slice(0, 1)[0]
  const rest = sections.slice(1)

  return (
    <nav className="header-nav">
      <ul className="nav-group">
        <NavLink className="gray" title={first} />
        {rest.map(section => (
          <NavLink key={section} title={section} />
        ))}
      </ul>
    </nav>
  )
}

export default Nav
