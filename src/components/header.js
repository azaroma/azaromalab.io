import PropTypes from "prop-types"
import React from "react"
import Nav from "../components/nav"
import "./header.css"

const Header = () => (
  <header>
    <Nav />
  </header>
)

Header.propTypes = {
  siteTitle: PropTypes.string,
}

Header.defaultProps = {
  siteTitle: ``,
}

export default Header
