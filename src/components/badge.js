import React from "react"
import "./badge.css"
import bat from "../images/bat-3.png"

const Badge = () => (
  <div className="badge">
    <h1 className="badge-text">azaroma</h1>
    <div>
      <img alt="bat" className="badge-image" src={bat} width={500} />
    </div>
  </div>
)

export default Badge
